# Libs.jl

An unorganised library of Julia functions which I need to be mobile.

This library contains both functions of a rather general interest, such as `iFunc()` as well as more specific ones, such as `plotPA()`.

This is not created for the purpose of sharing (although I would not mind doing so if you asked).
Any sub-library which develops into something of truly general interest should be moved to its own repository.

To install the library:
```julia
Pkg.clone("git@gitlab.com:slcu/teamHJ/niklas/Libs.jl.git")
```

To use the functions:
```julia
using Libs
```

Currently, this will provide the functions:
- `interactivePlot()`
- `plotPA()`
- `iPlot()`
- `iFunc()`
- `plotDE()`
