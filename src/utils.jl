"""
    `getEq(ode, u0)`

return the equilibrium of an ODE.
"""
function get_eq(ode, u0, param)
    #i = copy(ode.i)
    #ode.i = 0.
    sprob = SteadyStateProblem{true}(ode, u0, param)
    ssol = solve(sprob)
    #return [1.; ssol.u[2:end]]
    #ode.i=1
    ssol.retcode != :Success && warn("get_eq solution did not succeed")
    any(ssol.u .< 0) && warn("get_eq resulted in negative concentrations: $(ssol.u)")
    return ssol.u
end


function terminate_condition(t, u, i; rtol::Float64=1e-3, atol::Float64=1e-3)
    #all(abs.(i.u - i.uprev ) / i.dt .< atol) &&  all(abs.((i.u - i.uprev ) / i.dt .* u) .< rtol)
    all(abs.( get_du(i) ) .< atol) &&  all(abs.( get_du(i) ./ u) .< rtol)
end

derivs_small(ode::AbstractParameterizedFunction, u, param; tol=1e-3) = all( abs.(ode(u, param, 0.)) .< tol )
function derivs_small(ode::AbstractReactionNetwork, u, param; tol=1e-3)
    du = zeros(u)
    ode.f(du, u, param, 0.)
    all( abs.(du) .< tol )
end

function run_to_eq(ode, u0, param)

    #if :Amp in fieldnames(ode)
        #Amp = copy(ode.Amp)
        #ode.Amp = 0.
    #end

    tmax = 1e6 ## 1e6 minutes is about two years.
    cb = DiscreteCallback(terminate_condition, terminate!)
    prob = ODEProblem(ode, u0, (0.,tmax), param)
    sol = solve(prob, Rosenbrock23(); callback=cb, force_dtmin=true, maxiters=1e6)

    any((s.retcode != :Success for s in sol)) && warn("run_to_eq solution did not succeed")
    if sol.t[end] == tmax || !derivs_small(ode, sol.u[end], param)
        warn("run_to_eq did not reach a tolerable equilibrium.")
    end

    #:Amp in fieldnames(ode) && (ode.Amp = Amp)
    return sol.u[end]
end


"""
    logmap(n, min, max)

map a number n from U(0,1) to logU(10^min, 10^max).
"""
function logmap(p::Number, min, max)
    @assert 0 <= p <= 1 "logmap is intended to map from U(0,1), you have provided $p."
    @assert min < max "logmap needs the min argument to be smaller than the max."
    10 ^ ((max-min) * p + min )
end
