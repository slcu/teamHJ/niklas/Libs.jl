"""
# interactivePlot
Automatically generate interactive widgets for the parameters of an ODE.

### args

* ode :: defined using the @ode_def macro of DifferentialEquations.
* plotFunction :: a function which will be called as
			    plotFunction(ode; param=, plotVars=, kwargs...)

### kwargs (=default)
* newParam=false :: overwrite the parameters with ones.
* sliderRange=-10:0.1:5 :: range of values for all the widgets.
"""
function interactivePlot(ode, plotFunction, args...;
        param::Vector{Float64}=fill(1., length(ode.params)),
        plotVars=[:y], sliderRange=logspace(-5,3,101),
        cont_update=true,
        kwargs...)


    widgets = Vector{Interact.InputWidget}()
	push!(widgets, selection(ode.syms; value=plotVars, multi=true))
    push!(widgets, checkbox(label="Plot data", value=true))

    append!(widgets,
        [
            selection_slider(signif.(sliderRange, 3),
                label=latexify("$p"),
                value=param[i],
                continuous_update=cont_update)
            for (i, p) in enumerate(ode.params)
        ])

	paramButton = button("copy parameters")

    display(hbox(vbox(widgets[3:end]...), vbox(widgets[1:2]..., paramButton)))

	#saveName = textbox("plot.pdf")
	#saveButton = button("Save figure")
	#display(hbox(saveName, saveButton))
	#foreach(buttonClick->savefig(saveName.value), signal(saveButton))


	foreach(x->clipboard("param = $(getfield.(widgets[3:end], :value))"), signal(paramButton))


    map( (x...) ->
        plotFunction(ode, args...;
                    param=[i for i in x[3:length(ode.params)+2]],
    				plotVars = x[1],
                    plotData = x[2],
				 	kwargs...
            		),
        [signal(i) for i in widgets]...
       )
end
