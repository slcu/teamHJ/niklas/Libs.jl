__precompile__()
module Libs

using Interact
using DifferentialEquations
using Latexify
using Plots
using JLD
import Base: length, ones, zeros

ODEType = Union{AbstractParameterizedFunction, AbstractReactionNetwork}
length(ode::ODEType, field::Symbol) = length(getfield(ode, field))
ones(ode::ODEType, field::Symbol) = ones(length(ode, field))
zeros(ode::ODEType, field::Symbol) = zeros(length(ode, field))

export interactivePlot, plotPA, iFunc, iPlot, get_de_widgets, plotDE, ode_plot!,
        sde_plot!, sde_mc_plot!, sensitivity_plot!, run_to_eq, get_eq,
        ode_sim, sde_sim, sde_mc_sim, append_param!, get_params, logmap,
        ones, length, zeros, ODEType


include("interactivePlot.jl")
include("plotfunctions.jl")
include("iFunc.jl")
include("iPlot.jl")
include("plotDE.jl")
include("utils.jl")
include("param_io.jl")
include("simulators.jl")

end
