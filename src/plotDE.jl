

"""
    `plotDE()`

Simulate differential equations and plot the result.
"""
function plotDE end


function plotDE(args...)
    plt = plot()
    plotDE!(plt, args...)
    return plt
end


function plotDE(odearray::AbstractArray, args...)
    plot([plotDE(ode, args...) for ode in odearray]..., link=:both, layout=size(odearray, 1,2))
end

function plotDE(odearray::AbstractArray, noisearray::AbstractArray, args...)
    plot([plotDE(odearray[i], noisearray[i], args...) for i=1:length(odearray)]..., link=:both, layout=size(odearray, 1,2))
end


function plotDE!(plt, ode::AbstractParameterizedFunction, noise::Function,
        simulators::Array, params, relative_u0, ylimit, plotVars, tstop, args...)
    i_1 = 1.
    #set_param_values!(ode, params)

    #:i in fieldnames(ode) && (ode.i = i_1)
    localparams = copy(params)
    localparams[1] = i_1
    tspan = (0.,tstop)
    u0 = fill(1., length(ode.syms))
    #u0 = getEq(ode, u0)
    u0 = run_to_eq(ode, u0, localparams)

    u0 = u0 .* relative_u0
    #:i in fieldnames(ode) && (ode.i = params[1])
    localparams[1] = params[1]

    plotkwargs = Dict(
        :vars=>plotVars,
        :lw=>3
    )

    for simulator! in simulators
        simulator!(plt, ode, noise, u0, params, tspan, plotkwargs)
    end

    ylimit && plot!(ylim=[0.,-Inf])
    plot!(ylabel="Concentration", xlabel="Time")
    :i in fieldnames(ode) && (ode.i = i_1)
    return nothing
end



################################################################################
#                                  Simulators                                  #
################################################################################
"""
    `sde_mc_plot!(plt, ode, noise, u0, tspan, plotkwargs)`

Run a single SDE monte-carlo simulation and add to the plot plt.
"""
function sde_mc_plot!(plt, ode, noise, u0, param, tspan, plotkwargs)
    prob = SDEProblem(ode, noise, fill(1., length(ode.syms)), tspan, param)
    monte_prob = MonteCarloProblem(prob; prob_func = (x...)->assign_u0(u0, x...))
    sol = solve(monte_prob, num_monte=10, delta=1e1)
    summ = MonteCarloSummary(sol,linspace(tspan..., 100))
    plot!(
        plt,
        summ,
        idxs=[find(ode.syms .== x)[1] for x in plotkwargs[:vars]],
        labels=plotkwargs[:vars]
        )
    return [s[end] for s in sol]
end


"""
    `sde_plot!(plt, ode, noise, u0, tspan, plotkwargs)`

Run a single SDE simulation and add to the plot plt.
"""
function sde_plot!(plt, ode, noise, u0::AbstractArray, param::AbstractArray, tspan::Tuple, plotkwargs::Dict)
    prob = SDEProblem(ode, noise, u0, tspan, param, callback=PositiveDomain())
    sol = solve(prob, delta=1e1)
    plot!(plt, sol; plotkwargs...)
    return sol.u[end]
end


"""
    `ode_plot!(plt, ode, noise, u0, tspan, plotkwargs)`

Run a single ODE simulation and add to the plot plt.
"""
function ode_plot!(plt, ode, noise::Function, u0::AbstractArray, param::AbstractArray, tspan::Tuple, plotkwargs::Dict)
    ode_plot!(plt, ode, u0, param, tspan, plotkwargs)
end


function ode_plot!(plt, ode, u0::AbstractArray, param, tspan::Tuple, plotkwargs::Dict)
    prob = ODEProblem(ode, u0, tspan, param)
    sol = solve(prob, Rosenbrock23())
    plot!(plt, sol; plotkwargs...)
    return sol.u[end]
end

function sensitivity_plot!(plt, ode, noise::Function, u0::AbstractArray, param, tspan::Tuple, plotkwargs::Dict)
    sensitivity_plot!(plt, ode, u0, param, tspan, plotkwargs)
end

function sensitivity_plot!(plt, ode, u0::AbstractArray, param, tspan::Tuple, plotkwargs::Dict)
    prob = ODELocalSensitivityProblem(ode, u0, tspan, param)
    sol = solve(prob, DP8())
    l = sol.prob.indvars
    id = 3

    #plot!(plt, sol, vars=l*id+1:l*(1+id), title= id==0 ? "ode" : latexify(f.params[id]))
    nr_var = length(ode.syms)
    var_id = find(ode.syms .== plotkwargs[:vars][1])[1]

    labels = reshape(ode.params, (1,length(ode.params)))
    plot!(plt, sol, vars=nr_var+var_id:nr_var:nr_var*length(ode.params)+var_id, label=labels)
    return sol.u[end][1:length(ode.syms)]
end
